package dbchain

import (
	"fmt"
	"log"

	"github.com/boltdb/bolt"
)

const DBFILE = "blockchain.db"
const BLOCK_BUCKET = "blocks"

// DBChain keeps a sequence of Blocks
type DBChain struct {
	//Set the tip of the DBChain instance to the last block hash stored in the DB.
	//We don’t store all the blocks in it anymore, instead only the tip of the chain is stored.
	tip []byte
	db  *bolt.DB
}

// DBChainIterator is used to iterate over DBChain blocks
type DBChainIterator struct {
	currentHash []byte
	db          *bolt.DB
}

func (dbc *DBChain) Close() {
	dbc.db.Close()
}

// AddBlock saves provided data as a block in the DBChain
func (dbc *DBChain) AddBlock(data string) {
	var lastHash []byte

	err := dbc.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(BLOCK_BUCKET))
		lastHash = b.Get([]byte("l"))

		return nil
	})

	if err != nil {
		log.Panic(err)
	}

	newBlock := NewDBBlock(data, lastHash)

	err = dbc.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(BLOCK_BUCKET))
		err := b.Put(newBlock.Hash, newBlock.Serialize())
		if err != nil {
			log.Panic(err)
		}

		err = b.Put([]byte("l"), newBlock.Hash)
		if err != nil {
			log.Panic(err)
		}

		dbc.tip = newBlock.Hash

		return nil
	})
}

// Iterator ...
func (dbc *DBChain) Iterator() *DBChainIterator {
	bci := &DBChainIterator{dbc.tip, dbc.db}

	return bci
}

// Next returns prev block starting from the tip
func (i *DBChainIterator) Next() *DBBlock {
	var blk *DBBlock

	err := i.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(BLOCK_BUCKET))
		encodedBlock := b.Get(i.currentHash)
		blk = DeserializedDBBlock(encodedBlock)

		return nil
	})

	if err != nil {
		log.Panic(err)
	}

	i.currentHash = blk.PrevBlockHash

	return blk
}

func NewGenesisBlock() *DBBlock {
	return NewDBBlock("Genesis Block, God", []byte{})
}

// NewDBChain creates a new Blockchain with genesis Block
func NewDBChain() *DBChain {
	var tip []byte
	db, err := bolt.Open(DBFILE, 0600, nil)
	if err != nil {
		log.Panic(err)
	}

	err = db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(BLOCK_BUCKET))

		if b == nil {
			fmt.Println("No existing blockchain found. Creating a new one...")
			genesis := NewGenesisBlock()

			b, err := tx.CreateBucket([]byte(BLOCK_BUCKET))
			if err != nil {
				log.Panic(err)
			}

			err = b.Put(genesis.Hash, genesis.Serialize())
			if err != nil {
				log.Panic(err)
			}

			err = b.Put([]byte("l"), genesis.Hash)
			if err != nil {
				log.Panic(err)
			}
			tip = genesis.Hash
		} else {
			tip = b.Get([]byte("l"))
		}

		return nil
	})

	if err != nil {
		log.Panic(err)
	}

	bc := &DBChain{tip, db}

	return bc
}
