package dbchain

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"strconv"
)

type DBBlock struct {
	Timestamp     int64
	Data          []byte
	PrevBlockHash []byte
	Hash          []byte
	Nonce         int
}

func (b *DBBlock) SetHash() {
	timestamp := []byte(strconv.FormatInt(b.Timestamp, 10))

	//[][]byte{}: array slice
	headers := bytes.Join([][]byte{b.PrevBlockHash, b.Data, timestamp}, []byte{})
	hash := sha256.Sum256(headers)

	b.Hash = hash[:] //[:]基于数组所有元素创建切片
}

func (b *DBBlock) Serialize() []byte {
	var result bytes.Buffer
	encoder := gob.NewEncoder(&result)

	encoder.Encode(b)

	return result.Bytes()
}

func DeserializedDBBlock(d []byte) *DBBlock {
	var block DBBlock

	decoder := gob.NewDecoder(bytes.NewReader(d))
	decoder.Decode(&block)

	return &block
}
