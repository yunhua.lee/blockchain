package dbchain

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"math"
	"math/big"
	"strconv"
	"time"
)

const targetBits = 24

type DBPoW struct {
	block  *DBBlock
	target *big.Int
}

func NewDBPoW(b *DBBlock) *DBPoW {
	target := big.NewInt(1)
	target.Lsh(target, uint(256-targetBits))

	pow := &DBPoW{b, target}

	return pow
}

func (pow *DBPoW) prepareData(nonce int) []byte {
	data := bytes.Join(
		[][]byte{
			pow.block.PrevBlockHash,
			pow.block.Data,
			IntToHex(pow.block.Timestamp),
			IntToHex(int64(targetBits)),
			IntToHex(int64(nonce)),
		},
		[]byte{},
	)

	return data
}

func IntToHex(n int64) []byte {
	return []byte(strconv.FormatInt(n, 16))
}

func (pow *DBPoW) Run() (int, []byte) {
	var hashInt big.Int
	var hash [32]byte
	nonce := 0

	fmt.Printf("Mining the block containing \"%s\"\n", pow.block.Data)

	for nonce < math.MaxInt64 {
		data := pow.prepareData(nonce)
		hash = sha256.Sum256(data)

		fmt.Printf("\r%x", hash)
		hashInt.SetBytes(hash[:])

		if hashInt.Cmp(pow.target) == -1 {
			break
		} else {
			nonce++
		}
	}

	fmt.Print("\n\n")

	return nonce, hash[:]
}

func NewDBBlock(data string, prevHash []byte) *DBBlock {
	//[]byte(data)是类型转换，[]byte{}是创建切片
	block := &DBBlock{time.Now().Unix(), []byte(data), prevHash, []byte{}, 0}

	pow := NewDBPoW(block)
	nonce, hash := pow.Run()

	block.Hash = hash[:]
	block.Nonce = nonce

	return block
}

func (p *DBPoW) Validate() bool {
	var hashInt big.Int

	data := p.prepareData(p.block.Nonce)
	hash := sha256.Sum256(data)

	hashInt.SetBytes(hash[:])

	//这里的规则应该是为了简化而设计这么简单，实际上应该复杂很多，所以智能合约容易出问题
	isValid := hashInt.Cmp(p.target) == -1

	return isValid
}
