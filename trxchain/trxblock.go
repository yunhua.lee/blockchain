package trxchain

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"log"
	"math"
	"time"
)

var (
	maxNonce = math.MaxInt64
)

type TrxBlock struct {
	Timestamp     int64
	Transactions  []*Transaction
	PrevBlockHash []byte
	Hash          []byte
	Nonce         int
}

func (tb *TrxBlock) HashTransactions() []byte {
	var txHashes [][]byte
	var txHash [32]byte

	for _, tx := range tb.Transactions {
		txHashes = append(txHashes, tx.ID)
	}
	txHash = sha256.Sum256(bytes.Join(txHashes, []byte{}))

	return txHash[:]
}

// Serialize serializes the block
func (b *TrxBlock) Serialize() []byte {
	var result bytes.Buffer
	encoder := gob.NewEncoder(&result)

	err := encoder.Encode(b)
	if err != nil {
		log.Panic(err)
	}

	return result.Bytes()
}

// DeserializeBlock deserializes a block
func DeserializeTrxBlock(d []byte) *TrxBlock {
	var block TrxBlock

	decoder := gob.NewDecoder(bytes.NewReader(d))
	err := decoder.Decode(&block)
	if err != nil {
		log.Panic(err)
	}

	return &block
}

// NewBlock creates and returns Block
func NewTrxBlock(transactions []*Transaction, prevBlockHash []byte) *TrxBlock {
	block := &TrxBlock{time.Now().Unix(), transactions, prevBlockHash, []byte{}, 0}
	p := NewTrxPoW(block)
	nonce, hash := p.Run()

	block.Hash = hash[:]
	block.Nonce = nonce

	return block
}

// NewGenesisBlock creates and returns genesis Block
func NewTrxGenesisBlock(coinbase *Transaction) *TrxBlock {
	return NewTrxBlock([]*Transaction{coinbase}, []byte{})
}
