package trxchain

import (
	"blockchain/utils"
	"bytes"
	"crypto/sha256"
	"fmt"
	"math/big"
)

const targetBits = 24

type TrxPoW struct {
	block  *TrxBlock
	target *big.Int
}

func NewTrxPoW(b *TrxBlock) *TrxPoW {
	target := big.NewInt(1)
	target.Lsh(target, uint(256-targetBits))

	pow := &TrxPoW{b, target}

	return pow
}

func (pow *TrxPoW) prepareData(nonce int) []byte {
	data := bytes.Join(
		[][]byte{
			pow.block.PrevBlockHash,
			pow.block.HashTransactions(),
			utils.IntToHex(pow.block.Timestamp),
			utils.IntToHex(int64(targetBits)),
			utils.IntToHex(int64(nonce)),
		},
		[]byte{},
	)

	return data
}

// Validate validates block's PoW
func (pow *TrxPoW) Validate() bool {
	var hashInt big.Int

	data := pow.prepareData(pow.block.Nonce)
	hash := sha256.Sum256(data)
	hashInt.SetBytes(hash[:])

	isValid := hashInt.Cmp(pow.target) == -1

	return isValid
}

// Run performs a proof-of-work
func (pow *TrxPoW) Run() (int, []byte) {
	var hashInt big.Int
	var hash [32]byte
	nonce := 0

	fmt.Printf("Mining a new block")
	for nonce < maxNonce {
		data := pow.prepareData(nonce)

		hash = sha256.Sum256(data)
		fmt.Printf("\r%x", hash)
		hashInt.SetBytes(hash[:])

		if hashInt.Cmp(pow.target) == -1 {
			break
		} else {
			nonce++
		}
	}
	fmt.Print("\n\n")

	return nonce, hash[:]
}
