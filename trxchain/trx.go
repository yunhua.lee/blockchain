package trxchain

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"encoding/hex"
	"fmt"
	"log"
)

const subsidy = 1000

//TXOutput that store “coins” (notice the Value field above)
//And storing means locking them with a puzzle, which is stored in the ScriptPubKey
//One important thing about outputs is that they are indivisible
type TXOutput struct {
	//And if its value is greater than required, a change is generated and sent back to the sender.
	Value int

	//And storing means locking them with a puzzle, which is stored in the ScriptPubKey
	//ScriptPubKey will store an arbitrary string (user defined wallet address).
	ScriptPubKey string
}

//an input references a previous output
//
type TXInput struct {
	Txid []byte //Txid stores the ID of such transaction
	Vout int    //Vout stores an index of an output in the transaction

	//ScriptSig is a script which provides data to be used in an output’s ScriptPubKey
	//If the data is correct, the output can be unlocked, and its value can be used to generate new outputs;
	//if it’s not correct, the output cannot be referenced in the input.
	//ScriptSig will store just an arbitrary user defined wallet address.
	//这个就是
	ScriptSig string
}

//When a miner starts mining a block, it adds a coinbase transaction to it
//Transactions do not transfer money from one address to another.
//Transactions just lock values with a script, which can be unlocked only by the one who locked them.
type Transaction struct {
	ID   []byte
	Vin  []TXInput
	Vout []TXOutput
}

func (tx *Transaction) Print() {
	fmt.Printf("ID: %s\n", hex.EncodeToString(tx.ID))

	for _, in := range tx.Vin {
		fmt.Printf("TXI: Txid=%x\n", hex.EncodeToString(in.Txid))
		fmt.Printf("TXI: Vout=%d\n", in.Vout)
		fmt.Printf("TXI: ScriptSig=%s\n", in.ScriptSig)

	}

	for _, out := range tx.Vout {
		fmt.Printf("TXO=====================\n")
		fmt.Printf("TXO: Value=%d\n", out.Value)
		fmt.Printf("TXO: ScriptPubKey=%s\n", out.ScriptPubKey)
	}
}

// IsCoinbase checks whether the transaction is coinbase
func (tx Transaction) IsCoinbase() bool {
	return len(tx.Vin) == 1 && len(tx.Vin[0].Txid) == 0 && tx.Vin[0].Vout == -1
}

func NewCoinbaseTX(to, data string) *Transaction {
	if data == "" {
		data = fmt.Sprintf("Reward to '%s'", to)
	}

	txin := TXInput{[]byte{}, -1, data}
	txout := TXOutput{subsidy, to} //subsidy is the amount of reward
	tx := Transaction{nil, []TXInput{txin}, []TXOutput{txout}}
	tx.SetID()

	return &tx
}

// SetID sets ID of a transaction
func (tx *Transaction) SetID() {
	var encoded bytes.Buffer
	var hash [32]byte

	enc := gob.NewEncoder(&encoded)
	err := enc.Encode(tx)
	if err != nil {
		log.Panic(err)
	}
	hash = sha256.Sum256(encoded.Bytes())
	tx.ID = hash[:]
}

func (in *TXInput) CanUnlockOutputWith(unlockingData string) bool {
	return in.ScriptSig == unlockingData
}

func (out *TXOutput) CanBeUnlockedWith(unlockingData string) bool {
	return out.ScriptPubKey == unlockingData
}
