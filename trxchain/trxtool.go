package trxchain

import (
	"encoding/hex"
	"fmt"
	"log"
)

// NewUTXOTransaction creates a new transaction
func NewUTXOTransaction(from, to string, amount int, bc *TrxChain) *Transaction {
	var inputs []TXInput
	var outputs []TXOutput

	acc, validOutputs := bc.FindSpendableOutputs(from, amount)

	fmt.Printf("SpendableOutputs, acc=%d, outputs len=%d \n", acc, len(validOutputs))
	if acc < amount {
		log.Panic("ERROR: Not enough funds")
	}

	// Build a list of inputs
	for txid, outs := range validOutputs {
		txID, err := hex.DecodeString(txid)
		if err != nil {
			log.Panic(err)
		}

		for _, out := range outs {
			fmt.Printf("New TXI: out=%d, from=%s\n", out, from)
			input := TXInput{txID, out, from}
			inputs = append(inputs, input)
		}
	}

	// Build a list of outputs
	outputs = append(outputs, TXOutput{amount, to})
	if acc > amount {
		outputs = append(outputs, TXOutput{acc - amount, from}) // a change
	}

	tx := Transaction{nil, inputs, outputs}
	tx.SetID()

	return &tx
}
