package main

import (
	"blockchain/dbchain"
	"blockchain/merklechain"
	"blockchain/trxchain"
)

func dbmain() {
	c := dbchain.NewDBChain()
	defer c.Close()

	cmd := dbchain.NewDBCLI(c)
	cmd.Run()
}

func trxmain() {
	cli := trxchain.TrxCLI{}
	cli.Run()
}

func main() {
	cli := merklechain.CLI{}
	cli.Run()
}
