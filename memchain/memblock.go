package memchain

import (
	"bytes"
	"crypto/sha256"
	"strconv"
	"time"
)

type MemBlock struct {
	Timestamp     int64
	Data          []byte
	PrevBlockHash []byte
	Hash          []byte
}

func NewMemBlock(data string, prevBlockHash []byte) *MemBlock {
	block := &MemBlock{time.Now().Unix(), []byte(data), prevBlockHash, []byte{}}
	block.SetHash()
	return block
}

func (b *MemBlock) SetHash() {
	timestamp := []byte(strconv.FormatInt(b.Timestamp, 10))

	//[][]byte{}: array slice
	headers := bytes.Join([][]byte{b.PrevBlockHash, b.Data, timestamp}, []byte{})
	hash := sha256.Sum256(headers)

	b.Hash = hash[:] //[:]基于数组所有元素创建切片
}
