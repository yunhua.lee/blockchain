package memchain

type MemChain struct {
	blocks []*MemBlock
}

func (bc *MemChain) AddBlock(data string) {
	prevBlock := bc.blocks[len(bc.blocks)-1]
	newBlock := NewMemBlock(data, prevBlock.Hash)
	bc.blocks = append(bc.blocks, newBlock)
}

func (c *MemChain) newMemGenesisBlock() *MemBlock {
	return NewMemBlock("Genesis Block", []byte{})
}

func NewMemChain() *MemChain {
	mc := &MemChain{}
	b := mc.newMemGenesisBlock()
	mc.blocks = append(mc.blocks, b)

	return mc
}

func (c *MemChain) GetBlocks() []*MemBlock {
	return c.blocks
}
